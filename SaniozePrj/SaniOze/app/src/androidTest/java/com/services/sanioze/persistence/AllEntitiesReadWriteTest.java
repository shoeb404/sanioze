package com.services.sanioze.persistence;

import android.content.Context;
import android.util.Log;

import com.services.sanioze.persistence.dao.BookingDao;
import com.services.sanioze.persistence.dao.MachineRunDataDao;
import com.services.sanioze.persistence.dao.PayloadDao;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;
import com.services.sanioze.persistence.entities.MachineRunData;
import com.services.sanioze.persistence.entities.Payload;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class AllEntitiesReadWriteTest {
    private static final String TAG = "AllEntitiesReadWriteTes";
    private PayloadDao payloadDao;
    private MachineRunDataDao machineRunDataDao;
    private BookingDao bookingDao;
    private SaniozeDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, SaniozeDatabase.class).build();
        payloadDao = db.payloadDao();
        machineRunDataDao = db.machineRunDataDao();
        bookingDao = db.bookingDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void testMachineRunData() throws Exception {
        long id = 1;
        for (int i = 0; i < 5; i++) {
            Booking data = new Booking();
            data.setId(id++);
            data.setName("Tom "+id);;
            List<Booking> bookingList = new ArrayList<>();;
            bookingList.add(data);
            bookingDao.insert(bookingList);
        }

        id = 0;
        for (int i = 0; i < 5; i++) {
            MachineRunData data = new MachineRunData();

            data.setBookingId(++id);
            data.setStartTime(System.currentTimeMillis());
            data.setStopTime(0);
            if(id==2 ||id==4 ){
                data.setStopTime(System.currentTimeMillis()+10000);
            }
            machineRunDataDao.insert(data);
        }

        List<MachineRunData> machineRunDataList = machineRunDataDao.findAll();

        for(MachineRunData machineRunData: machineRunDataList){
            Log.i(TAG," Run Data Stop: "+machineRunData.getStopTime()+ "  BookingId "+machineRunData.getBookingId()+" upld "+machineRunData.isUploaded());
        }
        for (int i = 0; i < 20; i++) {
            Payload data = new Payload(1,"10 "+i,"30","40",System.currentTimeMillis());
            payloadDao.insert(data);
        }

        for (int i = 0; i < 20; i++) {
            Payload data = new Payload(2,"10 "+i,"30","40",System.currentTimeMillis());
            payloadDao.insert(data);
        }

        for (int i = 0; i < 20; i++) {
            Payload data = new Payload(3,"10 "+i,"30","40",System.currentTimeMillis());
            payloadDao.insert(data);
        }

        for (int i = 0; i < 20; i++) {
            Payload data = new Payload(4,"10 "+i,"30","40",System.currentTimeMillis());
            payloadDao.insert(data);
        }


        List<MachineRunAndPayload> list = machineRunDataDao.findPendingDataToUpload();
        //assertEquals(5,list.size());

        List<Payload> payloads = payloadDao.findByBookingId(1);
        assertEquals(20,payloads.size());

        //till now, one booking is completed, find all the info of this booking
        List<MachineRunAndPayload> data = machineRunDataDao.findPendingDataToUpload();

        for(MachineRunAndPayload d : data){
            MachineRunData m = d.getMachineRunData();
            Log.i(TAG," Booking id "+m.getBookingId()+" start "+m.getStartTime()+" stop "+m.getStopTime()+" upload "+m.isUploaded());
            List<Payload> ps = d.getPayloadList();
            for(Payload p : ps){
                Log.i(TAG," id "+p.getId() +" Bid "+p.getBookingId()+" Oz"+p.getOzoneInPpm());
            }
        }
        //update upload flags to true to delete the record
        MachineRunData machineRunData = machineRunDataDao.findByBookingId(2);
        Log.i(TAG," Run Data "+machineRunData.getStopTime()+ " "+machineRunData.getBookingId()+" upld "+machineRunData.isUploaded());
        machineRunData.setUploaded(true);
        machineRunDataDao.update(machineRunData);

        List<Payload> payloads1 = payloadDao.findAll();
        for(Payload payload : payloads1){
            Log.i(TAG,"id "+payload.getId() +" Bid "+payload.getBookingId());
        }

        Log.i(TAG," Size of list before "+machineRunDataDao.findAll().size()+" payload table "+payloadDao.findByBookingId(2).size());
        //delete all the records which are uploaded
        machineRunDataDao.deleteAllUploadedData();
//booking id 2 is deleted check whether FK rule is applied or not
        Log.i(TAG," Size of list after delete"+machineRunDataDao.findAll().size()+" payload table "+payloadDao.findByBookingId(2).size());
        payloads1 = payloadDao.findAll();
        for(Payload payload : payloads1){
            Log.i(TAG,"id "+payload.getId() +" Bid "+payload.getBookingId());
        }


    }

}
