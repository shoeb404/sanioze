package com.services.sanioze;

import android.content.Context;

import com.services.sanioze.http.HttpManager;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;
import com.services.sanioze.persistence.entities.Payload;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws IOException {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        HttpManager httpManager = new HttpManager(appContext);
        MachineRunAndPayload machineRunAndPayload = new MachineRunAndPayload();
        machineRunAndPayload.id = 1007;
        machineRunAndPayload.userId = "1014";
        List<Payload> payloadList = new ArrayList<>();
        payloadList.add(new Payload(1007, "10", "11", "12", System.currentTimeMillis()));
        payloadList.add(new Payload(1007, "10", "11", "12", System.currentTimeMillis()));
        payloadList.add(new Payload(1007, "10", "11", "12", System.currentTimeMillis()));
        machineRunAndPayload.setPayloadList(payloadList);
        httpManager.upload(machineRunAndPayload);

        //httpManager.syncFetchBookings("1014", "");

        //httpManager.login("Partner1@gmail.com", "test");
    }
}