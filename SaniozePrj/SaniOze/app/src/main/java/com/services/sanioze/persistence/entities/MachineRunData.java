package com.services.sanioze.persistence.entities;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "machine_run_data")
public class MachineRunData {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "booking_id")
    private long bookingId;

    private String productName;

    private String machineId;

    private String partnerId;

    private long startTime;

    private long stopTime;

    private boolean uploaded;

    @Ignore
    private List<Payload> payloadList;

    public MachineRunData(){}


    public MachineRunData(MachineRunAndPayload machineRunAndPayload){
        this.bookingId = machineRunAndPayload.getMachineRunData().bookingId;
        this.productName = machineRunAndPayload.getMachineRunData().productName;
        this.machineId = machineRunAndPayload.getMachineRunData().machineId;
        this.partnerId = machineRunAndPayload.getMachineRunData().partnerId;
        this.startTime = machineRunAndPayload.getMachineRunData().startTime;
        this.stopTime = machineRunAndPayload.getMachineRunData().stopTime;
        this.payloadList = machineRunAndPayload.getMachineRunData().payloadList;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public List<Payload> getPayloadList() {
        return payloadList;
    }

    public void setPayloadList(List<Payload> payloadList) {
        this.payloadList = payloadList;
    }
}
