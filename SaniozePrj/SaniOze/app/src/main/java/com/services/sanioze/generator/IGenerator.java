package com.services.sanioze.generator;

import com.services.sanioze.bt.DeviceConnectionState;

import androidx.lifecycle.LiveData;

public interface IGenerator {
    boolean connect(String address);
    void start(long bookingId);
    void stop();
    void fanOn();
    void fanOff();
    void disconnect();

    LiveData<DeviceConnectionState> connectionStatus();
    LiveData<GeneratorData> generatorData();

    boolean addListener(Generator.IGeneratorEventListener listener);
    boolean removeListener(Generator.IGeneratorEventListener listener);

    interface IGeneratorEventListener {
        void onDataReceived(GeneratorData data);
        void onConnectionStateChange(DeviceConnectionState connectionState);
    }
}
