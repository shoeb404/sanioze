package com.services.sanioze.ui.bookings;

import android.util.Log;

import com.services.sanioze.http.HttpManager;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.SaniozeRepository;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.User;
import com.services.sanioze.threadpool.IThreadPool;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PagedList;

/**
 * PagedList.BoundaryCallback class to know when to trigger the Network request for more data
 */
public class BookingBoundaryCallback extends PagedList.BoundaryCallback<Booking> implements
        HttpManager.Callback, SaniozeRepository.InsertCallback {
    //Constant used for logs
    private static final String LOG_TAG = BookingBoundaryCallback.class.getSimpleName();
    // Constant for the Number of items in a page to be requested from the Github API
    private static final int NETWORK_PAGE_SIZE = 50;
    private User mUser;
    private String token;
    private ISaniozeRepository mSaniozeRepository;
    private IHttpManager mHttpManager;
    private IThreadPool mThreadPool;
    // Keep the last requested page. When the request is successful, increment the page number.
    private int lastRequestedPage = 1;
    // Avoid triggering multiple requests in the same time
    private boolean isRequestInProgress = false;
    // LiveData of network errors.
    private MutableLiveData<String> networkErrors = new MutableLiveData<>();
    private ResultCallback mResultCallback;

    public BookingBoundaryCallback(User user, IHttpManager httpManager, ISaniozeRepository repository,
                                   IThreadPool threadPool, ResultCallback resultCallback) {
        this.mUser = user;
        this.mHttpManager = httpManager;
        this.mSaniozeRepository = repository;
        this.mThreadPool = threadPool;
        this.mResultCallback = resultCallback;
    }

    public LiveData<String> getNetworkErrors() {
        return networkErrors;
    }

    /**
     * Method to request data from Github API for the given search query
     * and save the results.
     *
     * @param userId The query to use for retrieving the repositories from API
     */
    private void requestAndSaveData(String userId) {
        //Exiting if the request is in progress
        if (isRequestInProgress) return;

        //Set to true as we are starting the network request
        isRequestInProgress = true;

        mHttpManager.asyncFetchBookings(userId, this, mUser.getToken());
    }

    /**
     * Called when zero items are returned from an initial load of the PagedList's data source.
     */
    @Override
    public void onZeroItemsLoaded() {
        Log.d(LOG_TAG, "onZeroItemsLoaded: Started");
        requestAndSaveData(mUser.getUserId());
    }

    /**
     * Called when the item at the end of the PagedList has been loaded, and access has
     * occurred within {@link PagedList.Config#prefetchDistance} of it.
     * <p>
     * No more data will be appended to the PagedList after this item.
     *
     * @param itemAtEnd The first item of PagedList
     */
    @Override
    public void onItemAtEndLoaded(@NonNull Booking itemAtEnd) {

    }

    /**
     * Callback invoked when the Search Repo API Call
     * completed successfully
     *
     * @param items The List of Repos retrieved for the Search done
     */
    @Override
    public void onSuccess(List<Booking> items) {
        Thread thread = new Thread(() -> {
            mSaniozeRepository.insert(items, this);
        });
        mThreadPool.submit(thread);
    }

    /**
     * Callback invoked when the Search Repo API Call failed
     *
     * @param errorMessage The Error message captured for the API Call failed
     */
    @Override
    public void onError(String errorMessage) {
        //Update the Network error to be shown
        networkErrors.postValue(errorMessage);
        //Mark the request progress as completed
        isRequestInProgress = false;
    }

    @Override
    public void insertFinished() {
        lastRequestedPage++;
        isRequestInProgress = false;
        mResultCallback.onResultSuccess();
    }

    interface ResultCallback {
        void onResultSuccess();
    }
}
