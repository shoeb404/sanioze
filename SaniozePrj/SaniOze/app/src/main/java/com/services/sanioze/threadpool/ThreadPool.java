package com.services.sanioze.threadpool;

import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadPool implements IThreadPool{

    private ExecutorService executorService = null;

    public ThreadPool(){
        Utility.logInfo("Toyama ","---- ThreadPool creation");
        executorService = Executors.newFixedThreadPool(Constant.THREAD_POOL_SIZE);
    }


    @Override
    public Future submit(Runnable runnable) {
        Utility.logInfo("Toyama ","submitting down executor "+executorService.isShutdown()+" "
                +executorService.isTerminated()+" this "+this);
        return executorService.submit(runnable);
    }

    public void shutdown(){
        Utility.logInfo("Toyama ","Shutting down executor");
        executorService.shutdown();
    }
}
