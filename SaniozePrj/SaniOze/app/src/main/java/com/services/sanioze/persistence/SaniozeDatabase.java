package com.services.sanioze.persistence;

import android.content.Context;

import com.services.sanioze.persistence.dao.BookingDao;
import com.services.sanioze.persistence.dao.MachineRunDataDao;
import com.services.sanioze.persistence.dao.PayloadDao;
import com.services.sanioze.persistence.dao.UserDao;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.MachineRunData;
import com.services.sanioze.persistence.entities.Payload;
import com.services.sanioze.persistence.entities.User;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import static com.services.sanioze.common.Constant.DB_NAME;


@Database(entities = {Booking.class, MachineRunData.class, Payload.class, User.class}, version = 1)
abstract class SaniozeDatabase extends RoomDatabase {

    private static SaniozeDatabase sInstance = null;
    public abstract UserDao userDao();
    public abstract BookingDao bookingDao();
	public abstract PayloadDao payloadDao();
	public abstract MachineRunDataDao machineRunDataDao();

    public static synchronized SaniozeDatabase getInstance(Context context){
        if (sInstance == null && context != null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(),
                    SaniozeDatabase.class, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return sInstance;
    }
}
