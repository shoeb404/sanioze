package com.services.sanioze.ui;

import com.services.sanioze.controller.ServiceController;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.User;
import com.services.sanioze.threadpool.IThreadPool;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ActivityViewModel extends ViewModel {

    private static final String TAG = "ActivityViewModel";
    private MutableLiveData<User> mUserMutableLiveData;

    public ActivityViewModel() {
        mUserMutableLiveData = new MutableLiveData<>();

        requestUserData();
    }

    public MutableLiveData<User> getEventMutableLiveData() {
        return mUserMutableLiveData;
    }

    private void requestUserData() {
        ServiceController serviceController = ServiceController.getInstance();
        IThreadPool threadPool = serviceController.getThreadPool();
        ISaniozeRepository saniozeRepository = serviceController.getSaniozeRepository();

        Thread userData = new Thread(() -> {
            User user = saniozeRepository.findUser();
            mUserMutableLiveData.postValue(user);
        });
        threadPool.submit(userData);
    }
}
