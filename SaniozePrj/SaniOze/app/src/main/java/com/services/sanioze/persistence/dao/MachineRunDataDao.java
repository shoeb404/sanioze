package com.services.sanioze.persistence.dao;

import com.services.sanioze.persistence.entities.MachineRunAndPayload;
import com.services.sanioze.persistence.entities.MachineRunData;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

@Dao
public interface MachineRunDataDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(MachineRunData machineRunData);

    @Update
    void update(MachineRunData machineRunData);

    @Delete
    void delete(MachineRunData machineRunData);

    @Query("DELETE FROM machine_run_data")
    void deleteAll();

    @Query("Select * from machine_run_data where booking_id = :bookingId")
    MachineRunData findByBookingId(long bookingId);

    @Query("Select * from machine_run_data")
    List<MachineRunData> findAll();

    //@Query("Select m.booking_id,m.machineId,m.partnerId,m.productName,m.startTime,m.stopTime from machine_run_data m Inner Join payload p On m.booking_id = p.booking_id where m.uploaded = true")
    //1 = true, 0=false

    @Transaction
    //@Query("Select * from machine_run_data where stopTime > 0 and uploaded = 0")
    @Query("Select * from machine_run_data")
    List<MachineRunAndPayload> findPendingDataToUpload();

    //@Query("Delete from machine_run_data where uploaded = 1")
    @Query("Delete from machine_run_data where uploaded = 1")
    void deleteAllUploadedData();
}
