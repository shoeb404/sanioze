package com.services.sanioze.http;

import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.LoggedInUser;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;

import java.io.IOException;
import java.util.List;

public interface IHttpManager {

    LoggedInUser login(String username, String password) throws IOException;

    void asyncFetchBookings(String userId, HttpManager.Callback callback, String token);

    List<Booking> syncFetchBookings(String userId, String token) throws IOException;
	
    boolean upload(MachineRunAndPayload machineRunAndPayload) throws IOException;
}
