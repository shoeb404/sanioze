package com.services.sanioze.ui.bookings;

import com.services.sanioze.controller.ServiceController;
import com.services.sanioze.datasource.factory.BookingDataFactory;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.User;
import com.services.sanioze.threadpool.IThreadPool;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

public class BookingsViewModel extends ViewModel implements BookingBoundaryCallback.ResultCallback {

    private LiveData<PagedList<Booking>> bookingList;
    private ISaniozeRepository mSaniozeRepository;
    private IHttpManager mHttpManager;
    private IThreadPool mThreadPool;
    private BookingDataFactory mBookingDataFactory;
    private User mUser;

    public BookingsViewModel(User user) {
        mUser = user;
        ServiceController serviceController = ServiceController.getInstance();
        mSaniozeRepository = serviceController.getSaniozeRepository();
        mHttpManager = serviceController.getHttpManager();
        mThreadPool = serviceController.getThreadPool();

        fetchData(user);
    }

    private void fetchData(User user) {
        if(user == null) {
            return;
        }

        PagedList.Config myPagingConfig = new PagedList.Config.Builder()
                .setPageSize(10)
                .setEnablePlaceholders(false)
                .build();

        mBookingDataFactory = new BookingDataFactory(mSaniozeRepository, mHttpManager, user);

/*        BookingBoundaryCallback bookingBoundaryCallback =
                new BookingBoundaryCallback(user, mHttpManager, mSaniozeRepository, mThreadPool, this);
        LiveData<String> networkErrors = bookingBoundaryCallback.getNetworkErrors();*/

        bookingList =
                new LivePagedListBuilder<>(mBookingDataFactory, myPagingConfig)
                        //.setBoundaryCallback(bookingBoundaryCallback)
                        .build();
    }

    public LiveData<PagedList<Booking>> getBookingList() {
        return bookingList;
    }

    public void refreshData(User user) {
        fetchData(user);
    }

    @Override
    public void onResultSuccess() {
        mBookingDataFactory.invalidateSource();
    }
}