package com.services.sanioze.generator;

import android.content.Context;

import com.services.sanioze.bt.BluetoothController;
import com.services.sanioze.bt.DeviceConnectionState;
import com.services.sanioze.bt.IBluetoothController;
import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class Generator implements IGenerator {
    private static final String TAG = "Generator";
    private IBluetoothController bluetoothController;
    private MutableLiveData<DeviceConnectionState> connectionStatusLiveData = new MutableLiveData<>();
    private MutableLiveData<GeneratorData> dataLiveData = new MutableLiveData<>();
    private List<IGeneratorEventListener> listeners = new ArrayList<>();
    private long bookingId = Long.MIN_VALUE;

    public Generator(Context ctx)  {
        this.bluetoothController =  new BluetoothController(ctx,bleListener);
    }

    @Override
    public boolean connect(String address) {
        if(bluetoothController.initialize()) {
            Utility.logInfo(TAG, "connect " + address);
            return bluetoothController.connect(address);
        }else{
            Utility.logInfo(TAG, "unable to init ble, chk ble is supported or not " + address);
        }
        return false;
    }

    @Override
    public void start(long bookingId) {
        Utility.logInfo(TAG, "start "+bookingId);
        if(bluetoothController.isConnected()) {
            bluetoothController.send(new byte[]{Constant.OZONE_SWITCH_ON});
            this.bookingId = bookingId;
        }
    }

    @Override
    public void stop() {
        Utility.logInfo(TAG, "stop ");
        if(bluetoothController.isConnected()) {
            bluetoothController.send(new byte[]{Constant.OZONE_SWITCH_OFF});
            bookingId = Long.MIN_VALUE;
        }
    };

    @Override
    public void fanOn() {
        Utility.logInfo(TAG, "fanOn ");
        if(bluetoothController.isConnected()) {
            bluetoothController.send(new byte[]{Constant.FAN_SWITCH_ON});
        }
    }

    @Override
    public void fanOff() {
        Utility.logInfo(TAG, "fanOff ");
        if(bluetoothController.isConnected()) {
            bluetoothController.send(new byte[]{Constant.FAN_SWITCH_OFF});
        }
    }

    @Override
    public void disconnect() {
        bluetoothController.disconnect();
    }

    @Override
    public LiveData<DeviceConnectionState> connectionStatus() {
        return connectionStatusLiveData;
    }

    @Override
    public LiveData<GeneratorData> generatorData() {
        return dataLiveData;
    }

    @Override
    public boolean addListener(IGeneratorEventListener listener) {
        boolean success = false;
        if(listener != null){
            Utility.logInfo(TAG,"addListener "+listener);
            success = listeners.add(listener);
        }
        return success;
    }

    @Override
    public boolean removeListener(IGeneratorEventListener listener) {
        boolean success = false;
        if(listener != null){
            Utility.logInfo(TAG,"removeListener "+listener);
            success = listeners.remove(listener);
        }
        return success;
    }

    private BluetoothController.BleListener bleListener = new BluetoothController.BleListener() {

        @Override
        public void onCharacteristicChanged(byte[] data) {
            Utility.logInfo(TAG, "CharacteristicChanged, parsing pkt");
            publishCharacteristicChangedValues(data);
        }

        @Override
        public void onConnectionStateChange(DeviceConnectionState connectionState) {
            publishConnectionStatus(connectionState);
        }


    };

    private void publishCharacteristicChangedValues(byte[] rawValue) {
        if (rawValue != null && rawValue.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(rawValue.length);
            //TODO Check String str = new String(rawValue) instead of builder
            for(byte byteChar : rawValue) {
                stringBuilder.append(String.format("%c", byteChar));
            }
            String[] values = stringBuilder.toString().split(Constant.CHARACTERISTIC_CHANGED_PKT_DELIMITER);
            if(values != null && values.length >= Constant.CHARACTERISTIC_CHANGED_PKT_SIZE) {
                GeneratorData data = new GeneratorData(bookingId, values[Constant.OZONE_INDEX], values[Constant.TEMPERATURE_INDEX], values[Constant.HUMIDITY_INDEX],
                        values[Constant.BATTERY_VOLTAGE_INDEX], "");
                Utility.logInfo(TAG, "publishing CharacteristicChanged Values " + data);
                dataLiveData.postValue(data);
                for(IGeneratorEventListener listener : listeners){
                    listener.onDataReceived(data);
                }
            }else{
                GeneratorData data = new GeneratorData("failed to parse : " + stringBuilder.toString());
                dataLiveData.postValue(data);
                Utility.logInfo(TAG, "parsing of CharacteristicChangedValues failed "+values);
            }
        }
    }

    private void publishConnectionStatus(DeviceConnectionState connectionState){
        Utility.logInfo(TAG,"Live connectionState "+connectionState.connectionState);
        connectionStatusLiveData.postValue(connectionState);
        for(IGeneratorEventListener listener : listeners){
            listener.onConnectionStateChange(connectionState);
        }
    }

}
