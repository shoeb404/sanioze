package com.services.sanioze.configuration;

import com.services.sanioze.MainActivity;

import dagger.Component;

@Component
public interface ApplicationComponent {
    void inject(MainActivity activity);//Main activity needs injection
}
