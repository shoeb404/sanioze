package com.services.sanioze.generator;

public class GeneratorData {
    public final String temperature;
    public final String humidity;
    public final String o3value;
    public final String batteryVoltage;
    public final String motorStatus;
    public final String error;
	public final long bookingId;

    public GeneratorData(String error) {
        this.o3value = "";
        this.temperature = "";
        this.humidity = "";
        this.batteryVoltage = "";
        this.motorStatus = "";
        this.bookingId = Long.MIN_VALUE;
        this.error = error;
    }

    public GeneratorData(long bookingId, String o3value, String temperature, String humidity, String batteryVoltage, String motorStatus) {
        this.bookingId = bookingId;
        this.o3value = o3value;
        this.temperature = temperature;
        this.humidity = humidity;
        this.batteryVoltage = batteryVoltage;
        this.motorStatus = motorStatus;
        this.error = "";
    }

    @Override
    public String toString() {
        return "GeneratorData{" +
                "temperature='" + temperature + '\'' +
                ", humidity='" + humidity + '\'' +
                ", o3value='" + o3value + '\'' +
                ", batteryVoltage='" + batteryVoltage + '\'' +
                ", motorStatus='" + motorStatus + '\'' +
                '}';
    }
}
