package com.services.sanioze.ui.bookings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.services.sanioze.R;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.User;

import java.util.Optional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BookingsFragment extends Fragment implements View.OnClickListener {

    private User mUser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Optional.ofNullable(getArguments()).ifPresent(bundle -> mUser = bundle.getParcelable("user"));
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bookings, container, false);
        initAdapter(root);
        return root;
    }

    private void initAdapter(View rootView) {
        BookingsViewModel viewModel =
                new ViewModelProvider(this, new BookingsViewModelFactory(mUser)).get(BookingsViewModel.class);
        RecyclerView recyclerView = rootView.findViewById(R.id.bookings_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        BookingsAdapter adapter = new BookingsAdapter(this::onClick);
        Optional.ofNullable(viewModel.getBookingList()).
                ifPresent(pagedListLiveData -> pagedListLiveData.observe(getViewLifecycleOwner(), bookings -> {
                    adapter.submitList(null);
                    adapter.submitList(bookings);
                }));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if(tag != null && tag instanceof Booking) {
            Booking item = (Booking) tag;
            launchHomeScreen(item);
        }
    }

        private void launchHomeScreen(Booking item){
        Bundle bundle = new Bundle();
        bundle.putParcelable("booking", item);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.nav_home, bundle);
    }
}