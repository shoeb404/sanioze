package com.services.sanioze.datasource;

import com.services.sanioze.controller.ServiceController;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.entities.LoggedInUser;
import com.services.sanioze.ui.login.data.Result;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            IHttpManager httpManager = ServiceController.getInstance().getHttpManager();
            LoggedInUser user = httpManager.login(username, password);
            return user == null ? new Result.Error(new Exception("Error logging in")) : new Result.Success<>(user);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}