package com.services.sanioze.bt;

public class DeviceConnectionState {
    public final int connectionState;
    public final String address;
    public final String name;

    public DeviceConnectionState(int connectionState, String address, String name) {
        this.connectionState = connectionState;
        this.address = address;
        this.name = name;
    }
}
