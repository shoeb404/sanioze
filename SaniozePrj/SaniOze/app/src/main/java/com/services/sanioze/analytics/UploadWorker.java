package com.services.sanioze.analytics;

import android.content.Context;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class UploadWorker extends Worker {

    public UploadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            MachineRunDataUpload.getInstance().upload();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.success();
    }
}
