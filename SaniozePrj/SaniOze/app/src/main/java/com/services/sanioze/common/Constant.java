package com.services.sanioze.common;

import com.services.sanioze.service.SaniozeService;

public class Constant {
    public static final String CHANNEL_ID = "sanioze_channel_01";
    public static final String RESULT_RECEIVER = "result_rx";
    public static final Class SERVICE_CLASS = SaniozeService.class;
    public static final int RESULT_RECEIVER_CODE = 555;

    public static final String SERVICE_UUID = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
    public static final String CHARACTERISTIC_UUID_TX = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
    public static final String CHARACTERISTIC_UUID_RX = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";
    public static final String DESCRIPTOR_UUID = "00002902-0000-1000-8000-00805f9b34fb";
    public static final String CHARACTERISTIC_CHANGED_PKT_DELIMITER = ",";
    public static final int CHARACTERISTIC_CHANGED_PKT_SIZE = 4;
    public static final int OZONE_INDEX = 0;
    public static final int TEMPERATURE_INDEX = 1;
    public static final int HUMIDITY_INDEX = 2;
    public static final int BATTERY_VOLTAGE_INDEX = 3;
    public static final int MOTOR_STATUS_INDEX = 4;
    public static final String OFF = "OF";
    public static final String ON = "ON";
    public static final boolean UPLOADED = true;
    public static final int DATA_SAVE_INTERVAL = 5;

    public static boolean MOTOR_STATUS_START = false;
    public static boolean MOTOR_STATUS_UPLOAD = false;

    public static byte OZONE_SWITCH_ON = 'a';//1=a
    public static byte OZONE_SWITCH_OFF = 'b';//3=c
    public static byte OZONE_SWITCH_ON_ACK = 0x0B;
    public static byte OZONE_SWITCH_OFF_ACK = 0x0D;

    public static byte FAN_SWITCH_ON= 'c';//2=b
    public static byte FAN_SWITCH_OFF = 'd';//4=d
    public static byte FAN_SWITCH_ON_ACK = 0x0C;
    public static byte FAN_SWITCH_OFF_ACK = 0x0E;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    public static final String DEGREE_CELSIUS ="\u2103";
    public static final String PM ="PM";

    //Http
    public static final String HTTP_BASE_URL = "https://sanioze.nanoton.in/";

    //Thread pool
    public static final int THREAD_POOL_SIZE = 6;

    //database
    public static final String DB_NAME = "sanioze_db";


}
