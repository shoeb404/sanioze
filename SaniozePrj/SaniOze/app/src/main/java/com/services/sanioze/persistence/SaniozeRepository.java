package com.services.sanioze.persistence;

import android.content.Context;

import com.services.sanioze.common.Utility;
import com.services.sanioze.persistence.dao.BookingDao;
import com.services.sanioze.persistence.dao.MachineRunDataDao;
import com.services.sanioze.persistence.dao.PayloadDao;
import com.services.sanioze.persistence.dao.UserDao;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;
import com.services.sanioze.persistence.entities.MachineRunData;
import com.services.sanioze.persistence.entities.Payload;
import com.services.sanioze.persistence.entities.User;

import java.util.List;
import java.util.Optional;


public class SaniozeRepository implements ISaniozeRepository {

    private SaniozeDatabase mSaniozeDatabase = null;
    private BookingDao mBookingDao;
    private UserDao mUserDao;
    private MachineRunDataDao machineRunDataDao;
    private PayloadDao payloadDao;

    public SaniozeRepository(Context context){
        mSaniozeDatabase = SaniozeDatabase.getInstance(context);
        mBookingDao = mSaniozeDatabase.bookingDao();
        mUserDao = mSaniozeDatabase.userDao();
        machineRunDataDao = mSaniozeDatabase.machineRunDataDao();
        payloadDao = mSaniozeDatabase.payloadDao();
    }

    public void close() {
        Utility.logInfo("Toyama ","closing toyama db");
        mSaniozeDatabase.close();
    }

    @Override
    public User findUser() {
        return mUserDao.findUser();
    }

    @Override
    public void insert(List<Booking> bookings, InsertCallback callback) {
        mBookingDao.insert(bookings);
        Optional.ofNullable(callback).ifPresent(insertCallback -> insertCallback.insertFinished());
    }

    @Override
    public List<Booking> bookingsByUserId(String userId) {
        return mBookingDao.bookingsByUserId();
    }

    @Override
    public void insert(User user) {
        mUserDao.insert(user);
    }

    @Override
    public void deleteAllBookings(){
        mBookingDao.deleteAll();
    }
	
    @Override
    public void saveMachineRunData(MachineRunData machineRunData) {
        if(machineRunDataDao != null && machineRunData != null){
            machineRunDataDao.insert(machineRunData);
        }
    }

    @Override
    public void updateMachineRunDataStopTime(long bookingId, long currentTimeMillis) {
        if(machineRunDataDao != null ){
            MachineRunData machineRunData = machineRunDataDao.findByBookingId(bookingId);
            machineRunData.setStopTime(currentTimeMillis);
            machineRunDataDao.update(machineRunData);
        }
    }

    @Override
    public void updateMachineRunDataUploadStatus(long bookingId, boolean uploaded) {
        if(machineRunDataDao != null ){
            machineRunDataDao.deleteAllUploadedData();
        }
    }

    @Override
    public void savePayload(Payload payload) {
        if(payloadDao != null){
            payloadDao.insert(payload);
            Utility.logInfo("Saniozerepo", "payload size after insert = " + payloadDao.findAll().size() + "");
        }
    }

    @Override
    public Optional<List<MachineRunAndPayload>> findPendingDataToUpload() {
        if(machineRunDataDao != null ){
            return Optional.ofNullable(machineRunDataDao.findPendingDataToUpload());
        }
        return Optional.empty();
    }

    @Override
    public void deleteAllUploadedMachineRunData() {

    }

    public interface InsertCallback {
        /**
         * Callback method invoked when the insert operation
         * completes.
         */
        void insertFinished();
    }
}
