package com.services.sanioze.bt;

public interface IBluetoothController {

    boolean connect(String address);

    boolean send(byte[] data);

    void disconnect();

    boolean isConnected();

    boolean initialize();
}
