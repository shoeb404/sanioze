package com.services.sanioze.controller;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.services.sanioze.generator.IGenerator;

import java.util.Optional;

public interface IServiceController {
    void startService(Context context);

    void stopService(Context context);

    boolean isServiceBound();

    IGenerator getGenerator();

    LiveData<Boolean> serviceBound();
}
