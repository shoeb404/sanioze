package com.services.sanioze.ui.home;

import com.services.sanioze.bt.DeviceConnectionState;
import com.services.sanioze.common.Utility;
import com.services.sanioze.controller.ServiceController;
import com.services.sanioze.generator.GeneratorData;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.MachineRunData;
import com.services.sanioze.threadpool.IThreadPool;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private static final String TAG = "HomeViewModel";
    private MutableLiveData<String> mText;
    private Booking mBooking;

    public HomeViewModel(Booking booking) {
        this.mBooking = booking;
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");

        saveBooking();
    }

    public LiveData<String> getText() {
        return mText;
    }

    public boolean connect(String address) {
        Utility.logInfo(TAG,"connect to "+address);
        if(ServiceController.getInstance().isServiceBound()) {
            return ServiceController.getInstance().getGenerator().connect(address);
        }
        return false;
    }

    public void start() {
        ServiceController.getInstance().getGenerator().start(mBooking.getId());
    }

    public void stop() {
        ServiceController.getInstance().getGenerator().stop();
    }

    public void fanOn() {
        ServiceController.getInstance().getGenerator().fanOn();
    }

    public void fanOff() {
        ServiceController.getInstance().getGenerator().fanOff();
    }

    public LiveData<GeneratorData> getGeneratorData() {
        return ServiceController.getInstance().getGenerator().generatorData();
    }

    public LiveData<DeviceConnectionState> getConnectionStatus() {
        return ServiceController.getInstance().getGenerator().connectionStatus();
    }

    public LiveData<Boolean> serviceStateObserver() {
        return ServiceController.getInstance().serviceBound();
    }

    public void disconnect() {
        Utility.logInfo(TAG,"req to disconnect() BT");
        ServiceController.getInstance().getGenerator().disconnect();
    }

    private void saveBooking() {
        ISaniozeRepository repository = ServiceController.getInstance().getSaniozeRepository();
        IThreadPool threadPool = ServiceController.getInstance().getThreadPool();
        MachineRunData machineRunData = new MachineRunData();
        machineRunData.setBookingId(mBooking.getId());
        machineRunData.setStartTime(System.currentTimeMillis());
        threadPool.submit(()->repository.saveMachineRunData(machineRunData));
    }
}