package com.services.sanioze.ui.home;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.services.sanioze.R;
import com.services.sanioze.bt.DeviceConnectionState;
import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;
import com.services.sanioze.generator.GeneratorData;
import com.services.sanioze.persistence.entities.Booking;

import java.util.Optional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import static com.services.sanioze.common.Constant.DEGREE_CELSIUS;
import static com.services.sanioze.common.Constant.PM;

/**
 * HomeFragment is provides UI to access the device
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "HomeFragment";
    private HomeViewModel homeViewModel;
    private TextView deviceConnStatusTv;
    private TextView deviceNameTv;

    private TextView temperatureTv;
    private TextView ozoneTv;
    private TextView humidityTv;
    private Booking mBooking;
	    private Button deviceConnectButton;
    private DeviceConnectionState deviceConnectionState;
    private BluetoothConnectActivityReceiver receiver ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBtPickerReceiver();
        Optional.ofNullable(getArguments()).ifPresent(bundle -> mBooking = bundle.getParcelable("booking"));
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        deviceConnStatusTv = root.findViewById(R.id.device_status);
        deviceNameTv = root.findViewById(R.id.device_name);

        Button startButton = root.findViewById(R.id.start_btn);
        startButton.setOnClickListener(this::onClick);
        Button stopButton = root.findViewById(R.id.stop_btn);
        stopButton.setOnClickListener(this::onClick);
        Button fanOnButton = root.findViewById(R.id.fan_on_btn);
        fanOnButton.setOnClickListener(this::onClick);
        Button fanOffButton = root.findViewById(R.id.fan_off_btn);
        fanOffButton.setOnClickListener(this::onClick);

        temperatureTv = root.findViewById(R.id.temperature_val_tv);
        humidityTv = root.findViewById(R.id.humidity_val_tv);
        ozoneTv = root.findViewById(R.id.ozone_val_tv);

        homeViewModel = new ViewModelProvider(this, new HomeViewModelFactory(mBooking)).get(HomeViewModel.class);
        homeViewModel.serviceStateObserver().observe(getViewLifecycleOwner(), serviceUp -> {
            Utility.logInfo(TAG,"Live service is up = "+serviceUp);
            if(serviceUp){
                registerForGeneratorEvents();
            }
        });

        deviceConnectButton = root.findViewById(R.id.device_conn_btn);
        deviceConnectButton.setOnClickListener(v -> launchBluetoothPicker());

        return root;
    }

/*    private void launch(){
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.action_nav_home_to_slideshowFragment);
    }*/

    private void launchBluetoothPicker() {
        Utility.logInfo(TAG, "enter launchBluetoothPicker() " + deviceConnectionState);
        if (deviceConnectionState != null) {
            if (deviceConnectionState.connectionState == Constant.STATE_CONNECTED || deviceConnectionState.connectionState == Constant.STATE_CONNECTING) {
                //show confirmation dialog
                Utility.logInfo(TAG, "Show alert dialog");
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.disconnect_device_dialog_title)
                        .setMessage(R.string.disconnect_device_dialog_msg)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                homeViewModel.disconnect();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } else {
                Utility.logInfo(TAG, "launch bt device picker");
                Intent intentBluetooth = new Intent();
                intentBluetooth.setAction("android.bluetooth.devicepicker.action.LAUNCH");
                intentBluetooth.putExtra("android.bluetooth.devicepicker.extra.LAUNCH_PACKAGE", "com.services.sanioze");
                //intentBluetooth.putExtra("android.bluetooth.devicepicker.extra.DEVICE_PICKER_LAUNCH_CLASS",HomeFragment.class.getName());
                startActivity(intentBluetooth);
            }
        }

    }

    private void registerBtPickerReceiver(){
        Utility.logInfo(TAG, "registerBtPickerReceiver");
        IntentFilter filter = new IntentFilter("android.bluetooth.devicepicker.action.DEVICE_SELECTED");
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        if(receiver == null){
            receiver = new BluetoothConnectActivityReceiver();
        }
        Context context = getContext();
        if(context != null && receiver != null){
            context.registerReceiver(receiver,filter);
        }
    }

    private void unregisterBtPickerReceiver(){
        Utility.logInfo(TAG, "unregisterBtPickerReceiver");
        Context context = getContext();
        if(context != null && receiver != null){
            context.unregisterReceiver(receiver);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.start_btn:
                onStartClick(view);
                break;
            case R.id.stop_btn:
                onStopClick(view);
                break;
            case R.id.fan_on_btn:
                onFanOnClick(view);
                break;
            case R.id.fan_off_btn:
                onFanOffClick(view);
                break;
        }
    }

    public class BluetoothConnectActivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent)  {
            if("android.bluetooth.devicepicker.action.DEVICE_SELECTED".equals(intent.getAction())) {
                Utility.logInfo(TAG,"onReceive(...)");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(device != null) {
                    Toast.makeText(context, "device " + device.getAddress(), Toast.LENGTH_SHORT).show();
                    Utility.logInfo(TAG,"onReceive(...) "+device.getAddress());
                    if (!homeViewModel.connect(device.getAddress())) {
                        Utility.logInfo(TAG,"onReceive(...) failed to connect "+device.getAddress());
                        Toast.makeText(context, "Failed to connect to device " + device.getAddress(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    private void updateDeviceConnectingViews(String name){
        deviceNameTv.setText(name);
        deviceConnStatusTv.setText(R.string.connecting);
        deviceConnectButton.setText(R.string.cancel);
    }

    private void updateDeviceConnectedViews(String name){
        deviceNameTv.setText(name);
        deviceConnStatusTv.setText(R.string.connected);
        deviceConnectButton.setText(R.string.disconnect);
    }

    private void updateDeviceDisconnectedViews(String name){
        Toast.makeText(getActivity(), "Device is not connected "+name, Toast.LENGTH_LONG).show();
        deviceNameTv.setText(R.string.default_bt_device_msg);
        deviceConnStatusTv.setText("");
        deviceConnectButton.setText(R.string.connect);
    }

    public void onStartClick(View v){
        homeViewModel.start();
        Constant.MOTOR_STATUS_START = true;
    }

    public void onStopClick(View v){
        homeViewModel.stop();
        Constant.MOTOR_STATUS_START = false;
    }

    public void onFanOnClick(View v){
        homeViewModel.fanOn();
    }

    public void onFanOffClick(View v){
        homeViewModel.fanOff();
    }

    private void registerForGeneratorEvents(){
        Utility.logInfo(TAG,"Live registerForGeneratorEvents() ");
        homeViewModel.getGeneratorData().observe(getViewLifecycleOwner(), new Observer<GeneratorData>() {
            @Override
            public void onChanged(@Nullable GeneratorData data) {
                Utility.logInfo(TAG,"Updating UI with new GeneratorData");
                temperatureTv.setText(data.temperature+DEGREE_CELSIUS);
                humidityTv.setText(data.humidity);
                ozoneTv.setText(data.o3value+PM);
            }
        });

        homeViewModel.getConnectionStatus().observe(getViewLifecycleOwner(), new Observer<DeviceConnectionState>() {
            @Override
            public void onChanged(@Nullable DeviceConnectionState connStatus) {
                Utility.logInfo(TAG,"Device conn "+connStatus.connectionState);
                deviceConnectionState = connStatus;
                switch (connStatus.connectionState){
                    case Constant.STATE_CONNECTED:
                        updateDeviceConnectedViews(connStatus.name);
                        break;
                    case Constant.STATE_CONNECTING:
                        updateDeviceConnectingViews(connStatus.name);
                        break;
                    case Constant.STATE_DISCONNECTED:
                        updateDeviceDisconnectedViews(connStatus.name);
                        break;
                    default:
                        Utility.logInfo(TAG,"Conn state is wrong "+connStatus);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBtPickerReceiver();
    }
}