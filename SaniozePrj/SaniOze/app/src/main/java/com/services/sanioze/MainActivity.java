package com.services.sanioze;

import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.services.sanioze.controller.ServiceController;
import com.services.sanioze.ui.ActivityViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private static final int[] DESTINATIONS_WITH_LOGIN = {R.id.nav_login, R.id.nav_bookings, R.id.nav_home};
    private static final int[] DESTINATIONS_WITHOUT_LOGIN = {R.id.nav_bookings, R.id.nav_home};

    private AppBarConfiguration mAppBarConfiguration;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ServiceController.getInstance().init(MainActivity.this.getApplicationContext());
        (new Thread(() -> ServiceController.getInstance().startService(MainActivity.this.getApplicationContext()))).start();


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActivityViewModel activityViewModel = new ViewModelProvider(this).get(ActivityViewModel.class);
        activityViewModel.getEventMutableLiveData().observe(this, user -> {
            boolean isLoggedIn = user != null && user.getToken() != null && !user.getToken().isEmpty();

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            AppBarConfiguration.Builder appBarConfigurationBuilder = new AppBarConfiguration.Builder(
                    isLoggedIn ? DESTINATIONS_WITHOUT_LOGIN : DESTINATIONS_WITH_LOGIN);
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            if (isLoggedIn) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("user", user);
                navController.setGraph(R.navigation.mobile_navigation_without_login, bundle);
                appBarConfigurationBuilder.setOpenableLayout(drawer);
            } else {
                navController.setGraph(R.navigation.mobile_navigation_with_login);
            }
            mAppBarConfiguration = appBarConfigurationBuilder.build();
            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(navigationView, navController);
        });
    }

/*    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}