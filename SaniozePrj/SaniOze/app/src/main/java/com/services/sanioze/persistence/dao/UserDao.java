package com.services.sanioze.persistence.dao;

import com.services.sanioze.persistence.entities.User;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user LIMIT 1")
    User findUser();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(User user);
}
