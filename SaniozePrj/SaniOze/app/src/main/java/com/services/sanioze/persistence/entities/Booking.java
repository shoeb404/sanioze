package com.services.sanioze.persistence.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "bookings")
public class Booking implements Parcelable {

    @PrimaryKey
    @SerializedName("serviceRequestId")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("createdUserId")
    @Expose
    private String createdUserId;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("modifiedUserId")
    @Expose
    private String modifiedUserId;
    @SerializedName("modifiedDate")
    @Expose
    private String modifiedDate;

    public Booking() {
    }

    protected Booking(Parcel in) {
        id = in.readLong();
        name = in.readString();
        phoneNo = in.readString();
        emailAddress = in.readString();
        deviceId = in.readString();
        location = in.readString();
        status = in.readString();
        summary = in.readString();
        userId = in.readString();
        createdUserId = in.readString();
        createdDate = in.readString();
        modifiedUserId = in.readString();
        modifiedDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(phoneNo);
        dest.writeString(emailAddress);
        dest.writeString(deviceId);
        dest.writeString(location);
        dest.writeString(status);
        dest.writeString(summary);
        dest.writeString(userId);
        dest.writeString(createdUserId);
        dest.writeString(createdDate);
        dest.writeString(modifiedUserId);
        dest.writeString(modifiedDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Booking> CREATOR = new Creator<Booking>() {
        @Override
        public Booking createFromParcel(Parcel in) {
            return new Booking(in);
        }

        @Override
        public Booking[] newArray(int size) {
            return new Booking[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
