package com.services.sanioze.datasource.factory;

import com.services.sanioze.datasource.BookingDataSource;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.User;

import androidx.paging.DataSource;

public class BookingDataFactory extends DataSource.Factory {

    private BookingDataSource bookingDataSource;
    private ISaniozeRepository mRepository;
    private IHttpManager mHttpManager;
    private User mUser;

    public BookingDataFactory(ISaniozeRepository repository, IHttpManager httpManager, User user) {
        this.mRepository = repository;
        this.mHttpManager = httpManager;
        this.mUser = user;
    }

    @Override
    public DataSource create() {
        bookingDataSource = new BookingDataSource(mRepository, mHttpManager, mUser);
        return bookingDataSource;
    }

    public void invalidateSource() {
        bookingDataSource.invalidate();
    }
}
