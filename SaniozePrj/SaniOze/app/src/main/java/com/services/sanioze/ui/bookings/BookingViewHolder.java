package com.services.sanioze.ui.bookings;

import android.view.View;
import android.widget.TextView;

import com.services.sanioze.R;
import com.services.sanioze.persistence.entities.Booking;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BookingViewHolder extends RecyclerView.ViewHolder {

    private View mView;

    public BookingViewHolder(@NonNull View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void bind(Booking item) {
        mView.setTag(item);
        TextView nameTv = mView.findViewById(R.id.name);
        TextView locationTv = mView.findViewById(R.id.locationValue);
        nameTv.setText(item.getName());
        locationTv.setText(item.getLocation());
    }
}
