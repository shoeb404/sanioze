package com.services.sanioze.configuration;

import android.app.Application;

public class SaniOzeApplication extends Application {
    ApplicationComponent appComponent = DaggerApplicationComponent.create();
}
