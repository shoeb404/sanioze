package com.services.sanioze.controller;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ResultReceiver;

import com.services.sanioze.analytics.MachineRunDataAnalyzer;
import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;
import com.services.sanioze.generator.IGenerator;
import com.services.sanioze.http.HttpManager;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.SaniozeRepository;
import com.services.sanioze.service.SaniozeService;
import com.services.sanioze.threadpool.IThreadPool;
import com.services.sanioze.threadpool.ThreadPool;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class ServiceController implements IServiceController {

    private static final String TAG = ServiceController.class.getSimpleName();
    private static ServiceController _instance = new ServiceController();

    private boolean mBound = false;
    private Optional<SaniozeService> mService;
    private Optional<CountDownLatch> mBoundCdLatch = null;
    private MutableLiveData<Boolean> boundToServiceLiveData = new MutableLiveData<>();
    private IHttpManager mIHttpManager;
    private ISaniozeRepository mISaniozeRepository;
    private IThreadPool mThreadPool;
    private MachineRunDataAnalyzer machineRunDataAnalyzer;

    public static ServiceController getInstance() {
        return _instance;
    }

    public void init(Context context) {
        mThreadPool = new ThreadPool();
        mIHttpManager = new HttpManager(context);
        mISaniozeRepository = new SaniozeRepository(context);
        machineRunDataAnalyzer = new MachineRunDataAnalyzer(context,mISaniozeRepository, mIHttpManager,mThreadPool,Constant.DATA_SAVE_INTERVAL);
    }

    @Override
    public void startService(Context ctx) {
        Optional<Context> context = Optional.ofNullable(ctx);

        if (!context.isPresent()) {
            Utility.logInfo(TAG, "Return. context is null");
            return;
        }

        final int LATCH_COUNT = 1;
        final int TIMEOUT = 5;

        boolean serviceStarted = isServiceStarted(context.get(), Constant.SERVICE_CLASS);
        if (!serviceStarted) {
            CountDownLatch countDownLatch = new CountDownLatch(LATCH_COUNT);
            ServiceStartedListener serviceStartedListener = new ServiceStartedListener(new Handler(Looper.getMainLooper()), countDownLatch);
            Intent intent = new Intent(context.get(), Constant.SERVICE_CLASS);
            intent.putExtra(Constant.RESULT_RECEIVER, serviceStartedListener);
            context.get().startService(intent);
            try {
                countDownLatch.await(TIMEOUT, TimeUnit.SECONDS);
                serviceStarted = true;
                Utility.logInfo(TAG, "unblocked: service is up!");
            } catch (InterruptedException e) {
                Utility.logInfo(TAG, "Return.Timeout while waiting for service to start");
                return;
            }
        }

        if (serviceStarted && !mBound) {
            CountDownLatch countDownLatch = new CountDownLatch(LATCH_COUNT);
            mBoundCdLatch = Optional.of(countDownLatch);
            Intent intent = new Intent(context.get(), SaniozeService.class);
            context.get().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            try {
                countDownLatch.await(TIMEOUT, TimeUnit.SECONDS);
                Utility.logInfo(TAG, "unblocked: service is bound!");
            } catch (InterruptedException e) {
                Utility.logInfo(TAG, "Return.Timeout while waiting for service to bound");
                return;
            }
        }

    }

    private boolean isServiceStarted(Context ctx, Class serviceClass) {
        Optional<Context> context = Optional.of(ctx);
        if (context.isPresent()) {
            ActivityManager manager = (ActivityManager) context.get().getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void stopService(Context ctx) {
        Utility.logInfo(TAG, "Unbind service, stopService() ");
        Optional<Context> context = Optional.of(ctx);
        context.ifPresent(c -> c.unbindService(mConnection));
    }

    @Override
    public boolean isServiceBound() {
        return mBound;
    }

    @Override
    public IGenerator getGenerator() {
        return mService.get().getGenerator();
    }

    @Override
    public LiveData<Boolean> serviceBound() {
        return boundToServiceLiveData;
    }

    private static class ServiceStartedListener extends ResultReceiver {
        private Optional<CountDownLatch> countDownLatch;

        public ServiceStartedListener(Handler handler, CountDownLatch cdLatch) {
            super(handler);
            this.countDownLatch = Optional.ofNullable(cdLatch);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == Constant.RESULT_RECEIVER_CODE && countDownLatch.isPresent()) {
                Utility.logInfo(TAG, "Service is started. latch.cd");
                countDownLatch.get().countDown();
            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            SaniozeService.SaniozeServiceBinder binder = (SaniozeService.SaniozeServiceBinder) service;
            mService = Optional.ofNullable(binder.getService());
            mBound = true;
            mBoundCdLatch.ifPresent(latch -> latch.countDown());
            if(mService.isPresent()) {
                machineRunDataAnalyzer.setGenerator(mService.get().getGenerator());
            }
            Utility.logInfo(TAG, "service is bound, onServiceConnected() ");
            boundToServiceLiveData.setValue(mBound);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Utility.logInfo(TAG, "service is unbound, onServiceDisconnected() ");
            boundToServiceLiveData.setValue(mBound);
        }
    };

    public IHttpManager getHttpManager() {
        return mIHttpManager;
    }

    public ISaniozeRepository getSaniozeRepository() {
        return mISaniozeRepository;
    }

    public IThreadPool getThreadPool() {
        return mThreadPool;
    }
}
