package com.services.sanioze.persistence;

import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;
import com.services.sanioze.persistence.entities.MachineRunData;
import com.services.sanioze.persistence.entities.Payload;
import com.services.sanioze.persistence.entities.User;

import java.util.List;
import java.util.Optional;

public interface ISaniozeRepository {

    User findUser();

    void insert(List<Booking> bookings, SaniozeRepository.InsertCallback callback);

    List<Booking> bookingsByUserId(String userId);

    public void deleteAllBookings();

    void insert(User user);
	
    void saveMachineRunData(MachineRunData machineRunData);

    void updateMachineRunDataStopTime(long bookingId, long currentTimeMillis);

    void updateMachineRunDataUploadStatus(long bookingId, boolean uploaded);

    void savePayload(Payload payload);

    Optional<List<MachineRunAndPayload>> findPendingDataToUpload();

    void deleteAllUploadedMachineRunData();

}
