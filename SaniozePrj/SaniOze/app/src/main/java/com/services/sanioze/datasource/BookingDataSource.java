package com.services.sanioze.datasource;

import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.User;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.ItemKeyedDataSource;

public class BookingDataSource extends ItemKeyedDataSource<Long, Booking> {

    private ISaniozeRepository mSaniozeRepository;
    private IHttpManager mHttpManager;
    private User mUser;

    public BookingDataSource(ISaniozeRepository repository, IHttpManager httpManager, User user) {
        mSaniozeRepository = repository;
        this.mHttpManager = httpManager;
        this.mUser = user;
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Booking> callback) {
        try {
            List<Booking> bookingList = mHttpManager.syncFetchBookings(mUser.getUserId(), mUser.getToken());
            if(bookingList != null && !bookingList.isEmpty()) {
                mSaniozeRepository.deleteAllBookings();
                mSaniozeRepository.insert(bookingList, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Booking> bookings = mSaniozeRepository.bookingsByUserId(mUser.getUserId());
        callback.onResult(bookings);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Booking> callback) {

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Booking> callback) {

    }

    @NonNull
    @Override
    public Long getKey(@NonNull Booking item) {
        return item.getId();
    }
}
