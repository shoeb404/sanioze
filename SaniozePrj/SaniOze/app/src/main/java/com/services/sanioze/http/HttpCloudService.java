package com.services.sanioze.http;

import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.LoggedInUser;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface HttpCloudService {

    @POST("LoginRest/login")
    Call<LoggedInUser> login(@Header("username") String username, @Header("password") String password);

    @GET("admin/ServiceOrderRest/ServiceOrder")
    Call<List<List<Booking>>> fetchBookings(@Header("userId") String userId, @Header("token") String token);

    @GET("admin/ServiceRequestRest/getServiceRequest")
    Call<List<List<Booking>>> fetchBookings(@Query("userId") String userId);

    @POST("admin/ServiceRequestRest/UpdateSummary")
    Call<ResponseBody> pushSessionSummary(@Query("userId") String userId, @Query("serviceRequestId") String serviceRequestId,
                                          @Query("summary") String summary);
}
