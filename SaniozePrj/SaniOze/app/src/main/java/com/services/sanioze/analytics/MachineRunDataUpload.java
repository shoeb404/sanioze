package com.services.sanioze.analytics;

import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;
import com.services.sanioze.persistence.entities.User;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class MachineRunDataUpload {

    private static final String TAG = "MachineRunDataUpload";
    private static MachineRunDataUpload sInstance = new MachineRunDataUpload();
    private IHttpManager httpManager;
    private ISaniozeRepository repository;

    private MachineRunDataUpload(){}

    public static MachineRunDataUpload getInstance(){
        return sInstance;
    }

    public void init(IHttpManager httpManager, ISaniozeRepository repository) {
        this.httpManager = httpManager;
        this.repository = repository;
    }

    public void upload() throws IOException {
        Utility.logInfo(TAG,"Uploading machine run data");
        if(httpManager != null && repository != null){
            Utility.logInfo(TAG,"Fetch all the eligible data");
            Optional<List<MachineRunAndPayload>> machineRunAndPayloads = repository.findPendingDataToUpload();
            if(machineRunAndPayloads.isPresent()){
                User user = repository.findUser();
                List<MachineRunAndPayload> payloadList = machineRunAndPayloads.get();
                Utility.logInfo(TAG,"Total data to upload "+payloadList.size());
                for(MachineRunAndPayload machineRunAndPayload : payloadList){
                    machineRunAndPayload.userId = user.getUserId();
                    boolean success = httpManager.upload(machineRunAndPayload);
                    if(success){
                        Utility.logInfo(TAG,"Data uploaded, update DB");
                        repository.updateMachineRunDataUploadStatus(machineRunAndPayload.getMachineRunData().getBookingId(), Constant.UPLOADED);
                    }else{
                        Utility.logInfo(TAG,"Failed to upload data");
                    }
                }
            }else{
                Utility.logInfo(TAG,"No data to upload data");
            }

            //data uploaded, now delete it from DB
            Utility.logInfo(TAG,"Delete all uploaded data from table");
            repository.deleteAllUploadedMachineRunData();

        }
    }
}
