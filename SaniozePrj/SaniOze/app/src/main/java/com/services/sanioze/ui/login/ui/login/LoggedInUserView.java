package com.services.sanioze.ui.login.ui.login;

import com.services.sanioze.persistence.entities.User;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private User user;
    //... other data fields that may be accessible to the UI

    LoggedInUserView(User user) {
        this.user = user;;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}