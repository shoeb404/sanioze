package com.services.sanioze.persistence.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.services.sanioze.persistence.entities.Payload;

import java.util.List;

@Dao
public interface PayloadDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Payload payload);

    @Update
    void update(Payload Payload);

    @Query("DELETE FROM payload where mach_run_booking_id = :bookingId")
    void delete(long bookingId);

    @Query("Select * FROM payload where mach_run_booking_id = :bookingId")
    List<Payload> findByBookingId(int bookingId);

    @Query("Select * from payload")
    List<Payload> findAll();
}
