package com.services.sanioze.http;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.services.sanioze.common.Constant;
import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.persistence.entities.LoggedInUser;
import com.services.sanioze.persistence.entities.MachineRunAndPayload;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpManager implements IHttpManager {

    private HttpCloudService mHttpCloudService;

    private Context mContext;

    public HttpManager(Context context) {
        mContext = context;

        startHttpService();
    }

    public void startHttpService() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.HTTP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        if (mHttpCloudService == null) {
            mHttpCloudService = retrofit.create(HttpCloudService.class);
        }
    }

    @Override
    public LoggedInUser login(String username, String password) throws IOException {
        Call<LoggedInUser> loggedInUserCall = mHttpCloudService.login(username, password);
        Response<LoggedInUser> loggedInUserResponse = loggedInUserCall.execute();
        return loggedInUserResponse.body();
    }

    @Override
    public void asyncFetchBookings(String userId, Callback callback, String token) {
        Call<List<List<Booking>>> bookingListCall = mHttpCloudService.fetchBookings(userId, token);
        bookingListCall.enqueue(new retrofit2.Callback<List<List<Booking>>>() {
            @Override
            public void onResponse(Call<List<List<Booking>>> call, Response<List<List<Booking>>> response) {
                callback.onSuccess(response.body().get(0));
            }

            @Override
            public void onFailure(Call<List<List<Booking>>> call, Throwable t) {
                callback.onError("" + t.getMessage());
            }
        });
    }

    @Override
    public List<Booking> syncFetchBookings(String userId, String token) throws IOException {
        Call<List<List<Booking>>> bookingListCall = mHttpCloudService.fetchBookings(userId);
        return bookingListCall.execute().body().get(0);
    }
	
    public boolean upload(MachineRunAndPayload machineRunAndPayload) throws IOException {
        Gson gson = new Gson();
        Call<ResponseBody> responseBodyCall = mHttpCloudService.pushSessionSummary(machineRunAndPayload.userId,
               String.valueOf( machineRunAndPayload.id),
                gson.toJson(machineRunAndPayload));
        Response<ResponseBody> responseBody = responseBodyCall.execute();
        return responseBody.code() == 200;
    }

    public interface Callback {

        void onSuccess(List<Booking> items);

        void onError(String errorMessage);
    }
}
