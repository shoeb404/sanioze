package com.services.sanioze.ui.home;

import com.services.sanioze.persistence.entities.Booking;
import com.services.sanioze.ui.bookings.BookingsViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
public class HomeViewModelFactory implements ViewModelProvider.Factory {

    private Booking mBooking;

    public HomeViewModelFactory(Booking booking) {
        this.mBooking = booking;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(mBooking);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}