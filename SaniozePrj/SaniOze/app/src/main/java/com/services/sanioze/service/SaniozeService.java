package com.services.sanioze.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import com.services.sanioze.MainActivity;
import com.services.sanioze.R;
import com.services.sanioze.common.Utility;
import com.services.sanioze.generator.Generator;
import com.services.sanioze.generator.IGenerator;

import java.util.Optional;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import static com.services.sanioze.common.Constant.CHANNEL_ID;

public class SaniozeService extends Service {
    public static final String TAG = SaniozeService.class.getSimpleName();
    private Optional<Notification> mNotification = null;
    private final IBinder mSaniozeBinder = new SaniozeServiceBinder();
    private Optional<IGenerator> mGenerator;

    @Override
    public void onCreate() {
        super.onCreate();
        Utility.logInfo(TAG,"onCreate channel creation");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        mNotification = Optional.ofNullable(new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.service_notification_msg))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build());
        Utility.logInfo(TAG,"onCreate completed");
        mGenerator = Optional.ofNullable(new Generator(this));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utility.logInfo(TAG,"starting service onStartCommand");
        if(mNotification.isPresent()) {
            Utility.logInfo(TAG,"starting fg service");
            startForeground(1, mNotification.get());
        }
        return START_NOT_STICKY;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String description = getString(R.string.channel_description);
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    description,
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Utility.logInfo(TAG,"onBind ");
        return mSaniozeBinder;
    }

    public IGenerator getGenerator() {
        return mGenerator.get();
    }

    public class SaniozeServiceBinder extends Binder{
        public SaniozeService getService() {
            return SaniozeService.this;
        }
    }
}
