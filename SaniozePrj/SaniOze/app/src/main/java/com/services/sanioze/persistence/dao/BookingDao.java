package com.services.sanioze.persistence.dao;


import com.services.sanioze.persistence.entities.Booking;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface BookingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Booking> bookings);

    @Query("SELECT * FROM bookings")
    List<Booking> bookingsByUserId();

    @Query("DELETE FROM bookings")
    void deleteAll();
}
