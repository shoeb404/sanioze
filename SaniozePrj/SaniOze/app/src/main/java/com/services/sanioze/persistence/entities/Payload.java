package com.services.sanioze.persistence.entities;

import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "payload",
        foreignKeys = @ForeignKey(entity = MachineRunData.class,
        parentColumns = "booking_id",
        childColumns = "mach_run_booking_id",
        onDelete = CASCADE))
public class Payload {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long id;

    @ColumnInfo(name = "mach_run_booking_id")
    private long bookingId;

    @SerializedName("ozoneInPpm")
    private String ozoneInPpm;

    @SerializedName("temperature")
    private String temperature;

    @SerializedName("humidity")
    private String humidity;

    private long timestamp;

    public Payload(long bookingId, String ozoneInPpm, String temperature, String humidity, long timestamp) {
        this.bookingId = bookingId;
        this.ozoneInPpm = ozoneInPpm;
        this.temperature = temperature;
        this.humidity = humidity;
        this.timestamp = timestamp;
    }

    public Payload() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public String getOzoneInPpm() {
        return ozoneInPpm;
    }

    public void setOzoneInPpm(String ozoneInPpm) {
        this.ozoneInPpm = ozoneInPpm;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
