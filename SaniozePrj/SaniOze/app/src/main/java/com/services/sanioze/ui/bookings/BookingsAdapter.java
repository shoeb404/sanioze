package com.services.sanioze.ui.bookings;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.services.sanioze.R;
import com.services.sanioze.persistence.entities.Booking;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

public class BookingsAdapter extends PagedListAdapter<Booking, BookingViewHolder> {

    private OnClickListener mOnClickListener;

    private static DiffUtil.ItemCallback<Booking> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Booking>() {
                @Override
                public boolean areItemsTheSame(@NonNull Booking oldItem, @NonNull Booking newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(@NonNull Booking oldItem, @NonNull Booking newItem) {
                    return oldItem.equals(newItem);
                }
            };

    protected BookingsAdapter(OnClickListener onClickListener) {
        super(DIFF_CALLBACK);

        mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public BookingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bookings_list_item, parent, false);
        view.setOnClickListener(mOnClickListener);
        return new BookingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookingViewHolder holder, int position) {
        holder.bind(getItem(position));
    }
}
