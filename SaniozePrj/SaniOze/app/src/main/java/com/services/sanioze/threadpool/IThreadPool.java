package com.services.sanioze.threadpool;

import java.util.concurrent.Future;

public interface IThreadPool {
    Future submit(Runnable runnable);
}
