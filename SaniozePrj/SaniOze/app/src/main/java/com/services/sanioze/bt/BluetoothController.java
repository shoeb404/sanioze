package com.services.sanioze.bt;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;

import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.bluetooth.BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE;
import static com.services.sanioze.common.Constant.STATE_CONNECTED;
import static com.services.sanioze.common.Constant.STATE_CONNECTING;
import static com.services.sanioze.common.Constant.STATE_DISCONNECTED;

public class BluetoothController implements IBluetoothController {

    private static final String TAG = BluetoothController.class.getSimpleName();
    private BluetoothManager bluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;


    private Context context;

    private BluetoothGattCharacteristic writeBluetoothGattCharacteristic;
    private BluetoothGattCharacteristic readBluetoothGattCharacteristic;
    private BluetoothGattDescriptor descriptor;
    private ExecutorService executor;
    private BleListener bleListener;

    public interface BleListener {
        void onCharacteristicChanged(byte[] data);

        void onConnectionStateChange(DeviceConnectionState connectionState);
    }


    public BluetoothController(Context context, BleListener listener) {
        this.context = context;
        bleListener = listener;
        publishConnState("","",mConnectionState);
    }

    public boolean initialize() {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            Utility.logInfo(TAG, "Unable to initialize BluetoothManager.");
            return false;
        }
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Utility.logInfo(TAG, "Unable to initialize mBluetoothAdapter.");
            return false;
        }
        return true;
    }

    @Override
    public boolean connect(String address) {
        if (mConnectionState == STATE_DISCONNECTED) {
            return connectTo(address);
        } else {
            Utility.logInfo(TAG, "n/a connect() curr state " + mConnectionState);
        }
        return false;
    }

    @Override
    public boolean send(byte[] data) {
        if (mConnectionState == STATE_CONNECTED) {
            if (executor == null || executor.isShutdown()) {
                Utility.logInfo(TAG, "executor is null or not up yet");
                return false;
            }
            Utility.logInfo(TAG, "send() command queued");
            executor.execute(() -> writeCharacteristic(data, writeBluetoothGattCharacteristic));
            return true;
        } else {
            Utility.logInfo(TAG, "send() wrong state " + mConnectionState);
            return false;
        }
    }

    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Utility.logInfo(TAG, "BluetoothAdapter not initialized");
            return;
        }
        Utility.logInfo(TAG,"calling BT gatt disconnect");
        mBluetoothGatt.disconnect();
    }

    @Override
    public boolean isConnected() {
        return mConnectionState == STATE_CONNECTED;
    }

    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
        executor.shutdown();
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTING;//Unless services are not found we'll not move to connected state
                Utility.logInfo(TAG, "Connected to GATT server." + (mBluetoothGatt == gatt));
                Utility.logInfo(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                mConnectionState = STATE_DISCONNECTED;
                shutDownExecutor();
                Utility.logInfo(TAG, "Disconnected from GATT server.");
                publishConnState(mBluetoothGatt, mConnectionState);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                isOurServiceAvailable();
            } else {
                Utility.logInfo(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Utility.logInfo(TAG,"-------------------- onCharacteristicWrite() ---------------------------");
            printChar(characteristic);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                final byte[] value = new byte[characteristic.getValue().length];
                System.arraycopy(characteristic.getValue(), 0, value, 0, characteristic.getValue().length);
                Utility.printHexString(value);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Utility.logInfo(TAG,"-------------------- onCharacteristicWrite() ---------------------------");
            printChar(characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Utility.logInfo(TAG,"-------------------- onCharacteristicChanged() ---------------------------");
            byte[] rawValue = Arrays.copyOf(characteristic.getValue(),characteristic.getValue().length);
            printChar(characteristic);
            bleListener.onCharacteristicChanged(rawValue);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            String serviceName = descriptor.getCharacteristic().getService().getUuid().toString();
            String charName =  descriptor.getCharacteristic().getUuid().toString();
            Utility.logInfo(TAG,"-------------------- onDescriptorRead() ---------------------------");
            Utility.logInfo(TAG,"onDescriptorRead () Service "+serviceName);
            Utility.logInfo(TAG,"onDescriptorRead () Characteristic "+charName);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            Utility.logInfo(TAG,"-------------------- onDescriptorWrite() ---------------------------");
            String serviceName = descriptor.getCharacteristic().getService().getUuid().toString();
            String charName =  descriptor.getCharacteristic().getUuid().toString();
            Utility.logInfo(TAG,"onDescriptorWrite () Service "+serviceName);
            Utility.logInfo(TAG,"onDescriptorWrite () Characteristic "+charName);
            if(status == BluetoothGatt.GATT_SUCCESS) {
                Utility.logInfo(TAG,"onDescriptorWrite () STATUS PASS ");
            }else {
                Utility.logInfo(TAG,"onDescriptorWrite () STATUS FAIL ");
            }
        }
    };

    private void printChar(BluetoothGattCharacteristic ch){
        byte[] rawValue = ch.getValue();
        String strValue = null;

        if (rawValue == null) {
            Utility.logInfo(TAG,"Characteristic value is null. Return");
            return;
        }

        if (rawValue.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(rawValue.length);
            for(byte byteChar : rawValue) {
                stringBuilder.append(String.format("%c", byteChar));
            }
            strValue = stringBuilder.toString();
            Utility.logInfo(TAG,"Characteristic value: "+strValue);
        }
        Utility.logInfo(TAG,"Characteristic value: "+strValue);
    }

    private void property(int value){
        for(int i=0;i<8;i++) {
            if (BigInteger.valueOf(value).testBit(i)) {
                switch(i){
                    case 0: Utility.logInfo(TAG,"Broadcast");break;
                    case 1: Utility.logInfo(TAG,"Read");break;
                    case 2: Utility.logInfo(TAG,"Write without response");break;
                    case 3: Utility.logInfo(TAG,"Write");break;
                    case 4: Utility.logInfo(TAG,"Notify");break;
                    case 5: Utility.logInfo(TAG,"Indicate");break;
                    case 6: Utility.logInfo(TAG,"Signed Write Command");break;
                    case 7: Utility.logInfo(TAG,"Queued Write");break;
                }
            }
        }
    }
    private void isOurServiceAvailable() {
        List<BluetoothGattService> services = mBluetoothGatt.getServices();
        displayGattServices(services);
        String uuid;

        boolean readCharacteristicFound = false;
        boolean writeCharacteristicFound = false;

        for (BluetoothGattService gattService : services) {
            uuid = gattService.getUuid().toString();
            Utility.logInfo(TAG, "Found Service UUID " + uuid);
            if (Constant.SERVICE_UUID.equalsIgnoreCase(uuid)) {
                Utility.logInfo(TAG, "Found Our Service UUID ");
                List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    uuid = gattCharacteristic.getUuid().toString();
                    Utility.logInfo(TAG, "Characteristic UUID " + uuid);
                    if (Constant.CHARACTERISTIC_UUID_RX.equalsIgnoreCase(uuid)) {
                        Utility.logInfo(TAG, "Read Characteristic Matched ");
                        readBluetoothGattCharacteristic = gattCharacteristic;
                        readCharacteristicFound = true;
                        List<BluetoothGattDescriptor> descs = gattCharacteristic.getDescriptors();
                        for(BluetoothGattDescriptor bluetoothGattDescriptor : descs) {
                            descriptor = bluetoothGattDescriptor;
                        }
                    }

                    if (Constant.CHARACTERISTIC_UUID_TX.equalsIgnoreCase(uuid)) {
                        Utility.logInfo(TAG, "Write Characteristic Matched ");
                        writeBluetoothGattCharacteristic = gattCharacteristic;
                        writeCharacteristicFound = true;
                    }
                }
                //found our service, now break outta loop
                break;
            }
        }

        if (readCharacteristicFound && writeCharacteristicFound) {
            Utility.logInfo(TAG, "BT connected successfully ");
            createExecutor();
            //enable async notification from ble
            setCharacteristicNotification(readBluetoothGattCharacteristic, true);
            mConnectionState = STATE_CONNECTED;
            publishConnState(mBluetoothGatt, mConnectionState);
        } else {
            Utility.logInfo(TAG, "unable to find our services, disconnecting bt ");
            disconnect();
        }

    }

    private boolean connectTo(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Utility.logInfo(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

       /* // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Utility.logInfo(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                publishConnState(mBluetoothGatt, mConnectionState);
                return true;
            } else {
                return false;
            }
        }*/

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Utility.logInfo(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        mConnectionState = STATE_CONNECTING;
        publishConnState(device.getName(), device.getAddress(),mConnectionState);

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(context, true, mGattCallback);
        Utility.logInfo(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;

        return true;
    }

    private void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (characteristic == null || mBluetoothAdapter == null || mBluetoothGatt == null) {
            Utility.logInfo(TAG, "setCharacteristicNotification() BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    private boolean writeCharacteristic(byte[] value, BluetoothGattCharacteristic characteristic) {

        //check mBluetoothGatt is available
        if (mBluetoothGatt == null || characteristic == null) {
            Utility.logInfo(TAG, "lost connection ");
            return false;
        }
        characteristic.setWriteType(WRITE_TYPE_NO_RESPONSE );
        characteristic.setValue(value);
        boolean status = mBluetoothGatt.writeCharacteristic(characteristic);
        Utility.logInfo(TAG,"Tx Data sent to peripheral "+status);
        Utility.printHexString(value);
        return status;
    }

    private void shutDownExecutor() {
        if (executor != null && !executor.isShutdown()) {
            executor.shutdownNow();
        }
    }

    private void createExecutor() {
        shutDownExecutor();
        executor = Executors.newSingleThreadExecutor();
    }

    private void publishConnState(BluetoothGatt bluetoothGatt, int state){
        if(bluetoothGatt != null && bluetoothGatt.getDevice()!= null){
            Utility.logInfo(TAG,"publishConnState ");
            publishConnState(bluetoothGatt.getDevice().getName(), bluetoothGatt.getDevice().getAddress(), state);
        }else{
            Utility.logInfo(TAG,"publishConnState gatt or device is null: "+bluetoothGatt);
            publishConnState("", "", state);
        }
    }

    private void publishConnState(String name, String address, int state) {
        Utility.logInfo(TAG,"publishing conn state "+name+" state "+state);
        if (name == null) {
            name = "";
        }

        if (address == null) {
            address = "";
        }
        bleListener.onConnectionStateChange(new DeviceConnectionState(state, address, name));
    }

    private void displayGattServices(List<BluetoothGattService> services) {
        String uuid;
        Utility.logInfo(TAG, "==================== GATT SERVICES ====================");
        for (BluetoothGattService gattService : services) {
            uuid = gattService.getUuid().toString();
            Utility.logInfo(TAG, "Found Service UUID " + uuid);
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                uuid = gattCharacteristic.getUuid().toString();
                Utility.logInfo(TAG, "Char UUID " + uuid);
                property(gattCharacteristic.getProperties());
                List<BluetoothGattDescriptor> descs = gattCharacteristic.getDescriptors();
                for(BluetoothGattDescriptor d : descs){
                    Utility.logInfo(TAG, "Desc UUID " + d.getUuid());
                    Utility.logInfo(TAG, "Desc value " + d.getValue());
                    Utility.logInfo(TAG, "Desc permission " + d.getPermissions());
                }
            }
        }
        Utility.logInfo(TAG, "========================================================");
    }
}
