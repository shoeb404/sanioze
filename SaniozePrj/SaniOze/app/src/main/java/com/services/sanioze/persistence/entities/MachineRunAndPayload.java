package com.services.sanioze.persistence.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

public class MachineRunAndPayload {

    @Embedded
    private MachineRunData machineRunData;

    //parentColumn = MachineRunData, entityColumn=FK of Payload, entity=class which is int list
    @Relation(parentColumn = "booking_id", entityColumn = "mach_run_booking_id", entity = Payload.class)
    @SerializedName("payload")
    private List<Payload> payloadList;

    @SerializedName("serviceRequestId")
    @Ignore
    public long id;

    @SerializedName("userId")
    @Ignore
    public String userId;

    @SerializedName("productName")
    @Ignore
    public String productName;

    @SerializedName("machineId")
    @Ignore
    public String machineId;

    @SerializedName("partnerId")
    @Ignore
    public String partnerId;

    @SerializedName("startTime")
    @Ignore
    public String startTime;

    @SerializedName("stopTime")
    @Ignore
    public String stopTime;

    public MachineRunData getMachineRunData() {
        return machineRunData;
    }

    public void setMachineRunData(MachineRunData machineRunData) {
        this.machineRunData = machineRunData;
        id = machineRunData.getBookingId();
        productName = machineRunData.getProductName();
        machineId = machineRunData.getMachineId();
        partnerId = machineRunData.getPartnerId();
        startTime = machineRunData.getStartTime()+"";
        stopTime = machineRunData.getStopTime()+"";
    }

    public MachineRunAndPayload() {
    }

    public List<Payload> getPayloadList() {
        return payloadList;
    }

    public void setPayloadList(List<Payload> payloadList) {
        this.payloadList = payloadList;
    }


}
