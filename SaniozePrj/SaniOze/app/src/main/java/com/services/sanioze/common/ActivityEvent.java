package com.services.sanioze.common;

public class ActivityEvent {

    private boolean isLoggedIn;

    public ActivityEvent(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }
}
