package com.services.sanioze.http;

import com.google.gson.annotations.Expose;
import com.services.sanioze.persistence.entities.Booking;

import java.util.List;

public class BookingsResponse {

    @Expose
    public List<List<Booking>> bookingList;
}
