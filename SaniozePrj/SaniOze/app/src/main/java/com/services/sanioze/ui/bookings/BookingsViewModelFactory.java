package com.services.sanioze.ui.bookings;

import com.services.sanioze.persistence.entities.User;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
public class BookingsViewModelFactory implements ViewModelProvider.Factory {

    private User mUser;

    public BookingsViewModelFactory(User user) {
        this.mUser = user;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(BookingsViewModel.class)) {
            return (T) new BookingsViewModel(mUser);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}