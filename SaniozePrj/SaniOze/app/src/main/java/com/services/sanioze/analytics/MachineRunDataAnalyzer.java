package com.services.sanioze.analytics;

import android.content.Context;

import com.services.sanioze.bt.DeviceConnectionState;
import com.services.sanioze.common.Constant;
import com.services.sanioze.common.Utility;
import com.services.sanioze.generator.GeneratorData;
import com.services.sanioze.generator.IGenerator;
import com.services.sanioze.http.IHttpManager;
import com.services.sanioze.persistence.ISaniozeRepository;
import com.services.sanioze.persistence.entities.Payload;
import com.services.sanioze.threadpool.IThreadPool;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

public class MachineRunDataAnalyzer {

    private static final String TAG = "MachineDataAnalyzer";
    private final Context context;

    private ISaniozeRepository repository;
    private IThreadPool threadPool;

    private long bookingId = Long.MIN_VALUE;
    private final int SAVE_TIME_INTERVAL_IN_SEC;
    private int dataCounter;

    public MachineRunDataAnalyzer(Context context, ISaniozeRepository repository, IHttpManager httpManager, IThreadPool threadPool, int saveTimeIntervalInSec){
        this.context = context;
        this.repository = repository;
        this.threadPool = threadPool;
        this.SAVE_TIME_INTERVAL_IN_SEC = saveTimeIntervalInSec;
        MachineRunDataUpload.getInstance().init(httpManager,repository);
    }

    public void setGenerator(IGenerator generator){
        Utility.logInfo(TAG,"Adding listener to generator");
        if(generator != null) {
            //it will be called every time app binds to service, so removing previous listener
            generator.removeListener(generatorEventListener);
            generator.addListener(generatorEventListener);
        }else{
            Utility.logInfo(TAG,"Failed to add listener to generator");
        }
    }


    private IGenerator.IGeneratorEventListener generatorEventListener = new IGenerator.IGeneratorEventListener() {
        @Override
        public void onDataReceived(GeneratorData data) {
            if(data != null) {
                if (/*bookingId != data.bookingId && Constant.ON.equalsIgnoreCase(data.motorStatus)*/
                        Constant.MOTOR_STATUS_START) {
                    Utility.logInfo(TAG, "Added Machine Run Data, ON "+data.bookingId);
                    //threadPool.submit(()->repository.saveMachineRunData(new MachineRunData(data.bookingId,"","","",System.currentTimeMillis(),0)));
                    bookingId = data.bookingId;
                    if(dataCounter % SAVE_TIME_INTERVAL_IN_SEC == 0) {
                        //storing every 5sec's data. update frequency can be reduced by buffering 30sec/1min's data
                        threadPool.submit(()->repository.savePayload(new Payload(data.bookingId,data.o3value,data.temperature,data.humidity, System.currentTimeMillis())));
                    }
                    dataCounter++;
                    Constant.MOTOR_STATUS_UPLOAD = false;
                }else if(/*bookingId == data.bookingId && Constant.OFF.equalsIgnoreCase(data.motorStatus)*/
                        !Constant.MOTOR_STATUS_START && !Constant.MOTOR_STATUS_UPLOAD){
                    Utility.logInfo(TAG, "Updated Machine Run Data, OFF "+bookingId);
                    threadPool.submit(()->repository.updateMachineRunDataStopTime(bookingId,System.currentTimeMillis()));
                    bookingId = Long.MIN_VALUE;
                    uploadWhenInternetIsAvailable();
                    Constant.MOTOR_STATUS_UPLOAD = true;
                    dataCounter = 0;
                }
            }
        }

        @Override
        public void onConnectionStateChange(DeviceConnectionState connectionState) {

        }
    };

    private void uploadWhenInternetIsAvailable() {
        Constraints constraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
        WorkRequest uploadWorkRequest =
                new OneTimeWorkRequest.Builder(UploadWorker.class)
                        .setConstraints(constraints)
                        .build();
        WorkManager.getInstance(context).enqueue(uploadWorkRequest);
    }
}
