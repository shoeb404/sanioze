package com.services.sanioze.common;

import android.util.Log;

public class Utility {
    public static void logInfo(String tag, String msg) {
        Log.i("sanioze_vik "+tag,msg);
    }

    private static final String TAG = "Utility";

    public static String getHexString(byte[] packet) {
        if(packet == null || packet.length <= 0){
            return "";
        }
        StringBuilder strBuilder = new StringBuilder();
        for(byte val : packet) {
            strBuilder.append(String.format("%02x ", val&0xff));
        }
        return strBuilder.toString();
    }

    public static void printHexString(byte[] value) {
        Utility.logInfo(TAG,getHexString(value));
    }
}
