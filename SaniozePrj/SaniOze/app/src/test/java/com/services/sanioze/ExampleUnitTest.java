package com.services.sanioze;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Test
    public void addition_isCorrect() throws InterruptedException {
        assertEquals(4, 2 + 2);
        String ss =null;
        Optional<String> o = Optional.ofNullable(ss);
        o.ifPresent(s->s.toLowerCase());
        System.out.println(o.orElse("hellow"));

        MutableLiveData<Integer> m = new MutableLiveData<>();
        m.setValue(10);
        m.setValue(20);
        m.setValue(3990);

        Thread.sleep(1000*1);

        m.observeForever( new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                System.out.println("Value : "+integer);
            }
        });

        Thread.sleep(1000*3);
        m.observeForever( new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                System.out.println("Value : "+integer);
            }
        });
        Thread.sleep(1000*5);
    }
}